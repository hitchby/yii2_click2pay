<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
      return $this->getData('index', ['articles']);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Get data from table.
     *
     * @param string $template
     *   Template name.
     * @param array $tables
     *   List of tables.
     *
     * @return string
     */
    public function getData($template, $tables)
    {
      $connection = \Yii::$app->db;

      $vars = [];
      foreach ($tables as $table) {
        $query = 'SELECT * FROM ' . $table . ';';
        $model = $connection->createCommand($query);
        $data = $model->queryAll();

        $vars[$table] = $data;
      }

      return $this->render($template, $vars);
    }

    /**
     * Displays articles page.
     *
     * @return string
     */
    public function actionArticles()
    {
      return $this->getData('articles', ['articles', 'links']);
    }

    /**
     * Displays links page.
     *
     * @return string
     */
    public function actionLinks()
    {
      return $this->getData('links', ['links', 'users']);
    }

    /**
     * Save article.
     *
     * @return string
     */
    public function actionSave_article()
    {
      $status = 'Error';
      if (isset($_GET['data']) && isset($_GET['data_text'])){
        $article_id = $_GET['data'];
        $text = $_GET['data_text'];

        // UPDATE article
        Yii::$app->db->createCommand()->update('articles', ['text' => $text], 'id = '.$article_id)->execute();
        $status = 'Success';
      }

      return $status;
    }

    /**
     * Get pay.
     *
     * @return string
     */
    public function actionGet_pay()
    {
      $status = 'Error';
      if (isset($_GET['data'])){
        $user_id = $_GET['data'];

        $connection = \Yii::$app->db;

        $query = 'SELECT summa FROM keepers WHERE user_id = '.$user_id.';';
        $model = $connection->createCommand($query);
        $summa = $model->queryOne();

        // UPDATE article
        Yii::$app->db->createCommand()->update('keepers', ['summa' => $summa['summa']-1], 'user_id = '.$user_id)->execute();
        $status = 'Success';
      }

      return $status;
    }
}
