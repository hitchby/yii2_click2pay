<?php
use yii\helpers\Html;
?>

<h1>Add user links:</h1>
<div class="links">
  <form>
    <?php
    foreach ($links as $link) {
      print '<div class="link">';
      print '<h3>'.$link['name'].'</h3>';

      print '<select class="users">';
      foreach ($users as $user) {
        print '<option data="'.$user['id'].'">'.$user['name'].'</option>';
      }
      print '</select>';

      print '<input type="text" class="link-text" value="'.$link['text'].'" />';
      print '<input data="'.$link['id'].'" class="link-save" type="submit" value="Save" />';
      print '</div>';
    }
    ?>
  </form>
</div>

<p>Debug info</p>
<pre><?= print_r($links) ?></pre>
<pre><?= print_r($users) ?></pre>