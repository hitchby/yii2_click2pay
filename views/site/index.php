<?php

/* @var $this yii\web\View */

$this->title = 'Click2pay Yii Application';
?>
<div class="site-index">
    <h1>List of articles:</h1>
    <div class="articles">
        <form>
          <?php
          foreach ($articles as $article) {
            print '<div class="article">';
            print '<h3>' . $article['name'] . '</h3>';
            print '<p>' . $article['text'] . '</p>';
            print '</div>';
          }
          ?>
        </form>
    </div>
</div>
