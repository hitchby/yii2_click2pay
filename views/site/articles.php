<?php
use yii\helpers\Html;
?>

<h1>Edit articles:</h1>
<div class="articles">
    <form>
      <?php
      foreach ($articles as $article) {
        print '<div class="article">';
        print '<h3>' . $article['name'] . '</h3>';

        print '<select class="links">';
        foreach ($links as $link) {
          print '<option data="' . $link['id'] . '">' . $link['name'] . '</option>';
        }

        print '</select>';
        print '<input type="button" class="put-link" value="Append this link" />';
        print '<textarea class="article-text">' . $article['text'] . '</textarea>';
        print '<input data="' . $article['id'] . '" class="article-save" type="submit" value="Save" />';
        print '</div>';
      }
      ?>
    </form>
</div>

<p>Debug info</p>
<pre><?= print_r($articles) ?></pre>