$(document).ready(function() {
  $('.article-save').on('click', function (e) {
    e.preventDefault();

    var article_text = $(this).siblings('.article-text').val();

    $.ajax({
      url: 'index.php',
      type: 'GET',
      data:  {
        r: 'site/save_article',
        data: $(this).attr('data'),
        data_text: article_text,
      },
      success: function (response) {
      },
      error: function (jqXHR, textStatus, errorThrown) {
      }
    });
  });

  $('.put-link').on('click', function (e) {
    e.preventDefault();

    var link_id = $(this).siblings('.links').find('option:selected').attr('data'),
        link_text = $(this).siblings('.links').find('option:selected').val(),
        link_html = '<a class=\"user-link\" data=\"' + link_id + '\" href=\"#\">' + link_text + '</a>',
        $txt = $(this).siblings('.article-text'),
        caretPos = $txt[0].selectionStart,
        textAreaTxt = $txt.val();

    $txt.val(textAreaTxt.substring(0, caretPos) + link_html + textAreaTxt.substring(caretPos) );
  });

  $('.user-link').on('click', function (e) {
    e.preventDefault();

    $.ajax({
      url: 'index.php',
      type: 'GET',
      data:  {
        r: 'site/get_pay',
        data: $(this).attr('data'),
      },
      success: function (response) {
      },
      error: function (jqXHR, textStatus, errorThrown) {
      }
    });

    return false;
  });
});